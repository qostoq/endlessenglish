﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMotor : MonoBehaviour
{

    public Transform LookAt;
    private bool smooth = true;
    private float smoothspeed = 0.125f;
    private Vector3 offset = new Vector3(0,3,-6f);
    
 

    

    // Use this for initialization
    void FixedUpdate()
    {
        

        Vector3 desiredposition = LookAt.transform.position + offset;

        if(transform.position.x >= 5f)
        {
            transform.position = new Vector3(5f, transform.position.y, transform.position.z);
            
        }

        if (transform.position.x <= -5f)
        {
            transform.position = new Vector3(-5f, transform.position.y, transform.position.z);

        }
        if (transform.position.y >= 10f)
        {
            transform.position = new Vector3(transform.position.x, 10f, transform.position.z);

        }
        

        if (smooth)
        { 
            
            
            transform.position = Vector3.Lerp(transform.position, desiredposition, smoothspeed);
            

        }
        else
        {
            transform.position = desiredposition;

        }



    }


}
