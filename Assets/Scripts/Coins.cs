﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Coins : MonoBehaviour {

    public Transform Player;
    public Quiz Say;
    public int CoinSpeed;

    public AudioClip coinsound;
    public AudioSource soundsource;

    // Use this for initialization
    void Start () {

        soundsource.clip = coinsound;

    }
	
	// Update is called once per frame
	void FixedUpdate () {

        transform.Rotate(0, 180 * Time.deltaTime, 0);
        
	}

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {

            Say.coinsayisi += 10;
            Debug.Log(Say.coinsayisi);
            Say.Csay.text = "Para: " + Say.coinsayisi;
            soundsource.Play();
            Destroy(gameObject);
            
            
        }

        
    }
    public void OnTriggerStay(Collider other)
    {
        if (other.name == "coinMagnetCollider" && Magnet.isMagnet == true)
        {

           
            transform.position = Vector3.MoveTowards(transform.position, Player.position, CoinSpeed * Time.deltaTime);

        }
    }



}
