﻿using UnityEngine;

[System.Serializable]

public class QuestionArray
{



    public string Soruismi, cevapa, cevapb, cevapc, cevapd;
    public int cevap;
    
    public Sprite Img;


    public QuestionArray(string Soru, string cevap1, string cevap2, string cevap3, string cevap4, int cp,Sprite img)
    {
        Img = img;
        
        Soruismi = Soru;
        cevapa = cevap1;
        cevapb = cevap2;
        cevapc = cevap3;
        cevapd = cevap4;
        cevap = cp;

    }

}