﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInGame : MonoBehaviour {

    private CharacterController Controller;

    public Quiz SoruControl;

    public Transform Player;

    

    //animasyon kontrol degiskenleri
    /*
    private bool inputL=false;
    private bool inputR=false;
    */
    private float animationDuration = 2.5f;
    public Animator anim;

    //klavye kontrol degiskenleri
    private KeyCode moveR;
    private KeyCode moveL;

    // karakter kontrol degiskenleri
    public float horizVel = 0;
    public float vertVel = 0;
    private int laneNum=2;
    private int laneNumVert = 0;
    private string controlLocked = "n";
    private bool altust = false;
    public static float speed = 20f;
    private Vector3 MoveVector;

    

    //SWİPE KONTROL DEGİSKENLERİ
    private Vector2 fingerDownPos;
    private Vector2 fingerUpPos;
    public bool detectSwipeAfterRelease = false;
    public float SWIPE_THRESHOLD = 20f;

    void OnTriggerEnter(Collider other)
    {
        if (other.name!="coin" && other.name != "coinMagnetCollider")
        {
            SoruControl.imageBool = true;
            SoruControl.showHideImages();
        }
        
    }

    void Start () {

        anim = GetComponent<Animator>();
        Controller = GetComponent<CharacterController>();
        speed = 20f;

    }
    
    // Update is called once per frame
    void FixedUpdate () {

        if (Time.timeSinceLevelLoad < animationDuration)//time.time; scene kaç sndir çalısıyor, bunu sn cinsinden degerini verir. kamera kontrolu
        {
            Controller.Move(Vector3.forward * speed * Time.deltaTime);
            return; // burda return dersen update fonk. un bundan sonrası çalısmaz updateden çıkar
        }

        
/*
        //editorden bu degiskenlere ulasmak icin kullanılır
        anim.SetBool("inputL", inputL);
        anim.SetBool("inputR", inputR);
        */
        //X  -- sağ-sol donus
        MoveVector.x = horizVel;
        MoveVector.y = vertVel;

            //s' e basıldı mı ---asagı
        if (Input.GetKeyDown(KeyCode.S) && laneNumVert == 1 && altust==true)
        {

            vertVel = -10;
            StartCoroutine(stopSlideVert());
            laneNumVert = 0;
            altust = false;

        }
        // yukarı
        if (Input.GetKeyDown(KeyCode.W) && laneNumVert == 0 && altust==false)
        {

            vertVel = 10;
            StartCoroutine(stopSlideVert());
            laneNumVert = 1;
            altust = true;

        }

        //sol
        if ((Input.GetKeyDown(KeyCode.A)) && (laneNum > 1) && (controlLocked == "n"))
        {

            horizVel = -12;
            
            StartCoroutine(stopSlide());
            /*
            inputL = true;
            */
            laneNum -= 1;
            controlLocked = "y";

            
            
        }
        //sag
        if ((Input.GetKeyDown(KeyCode.D)) && (laneNum < 3) && (controlLocked == "n"))
        {
            horizVel = 12;
            
            StartCoroutine(stopSlide());
            /*
            inputR = true;
            */
            laneNum += 1;
            controlLocked = "y";

            

        }
        
        //Z -- ileri doğru hareket
        MoveVector.z = speed;
        Controller.Move(MoveVector * Time.deltaTime);

        //swipe kontrol kod parcası
        foreach (Touch touch in Input.touches)
        {
            if (touch.phase == TouchPhase.Began)
            {
                fingerUpPos = touch.position;
                fingerDownPos = touch.position;
            }

            //Detects Swipe while finger is still moving on screen
            if (touch.phase == TouchPhase.Moved)
            {
                if (!detectSwipeAfterRelease)
                {
                    fingerDownPos = touch.position;
                    DetectSwipe();
                }
            }

            //Detects swipe after finger is released from screen
            if (touch.phase == TouchPhase.Ended)
            {
                fingerDownPos = touch.position;
                DetectSwipe();
            }
        }

    }


    IEnumerator stopSlide()
    {

        yield return new WaitForSeconds(.5f);
        horizVel = 0;
        controlLocked = "n";
        /*
        inputR = false;
        inputL = false;
        */

    }
    IEnumerator stopSlideVert()
    {

        yield return new WaitForSeconds(.5f);
        vertVel = 0;
        

    }

    void DetectSwipe()
    {

        if (VerticalMoveValue() > SWIPE_THRESHOLD && VerticalMoveValue() > HorizontalMoveValue())
        {
            Debug.Log("Vertical Swipe Detected!");

            //yukarı dogru swipe
            if (fingerDownPos.y - fingerUpPos.y > 0)
            {
                OnSwipeUp();
            }
            //asagı dogru swipe
            else if (fingerDownPos.y - fingerUpPos.y < 0)
            {
                OnSwipeDown();
            }
            fingerUpPos = fingerDownPos;

        }
        else if (HorizontalMoveValue() > SWIPE_THRESHOLD && HorizontalMoveValue() > VerticalMoveValue())
        {
            Debug.Log("Horizontal Swipe Detected!");
            //sag swipe
            if (fingerDownPos.x - fingerUpPos.x > 0)
            {
                OnSwipeRight();
            }
            //sol swipe
            else if (fingerDownPos.x - fingerUpPos.x < 0)
            {
                OnSwipeLeft();
            }
            fingerUpPos = fingerDownPos;

        }
        else {
            Debug.Log("No Swipe Detected!");
        }
    }

    float VerticalMoveValue()
    {
        return Mathf.Abs(fingerDownPos.y - fingerUpPos.y);
    }

    float HorizontalMoveValue()
    {
        return Mathf.Abs(fingerDownPos.x - fingerUpPos.x);
    }

    void OnSwipeUp()
    {
        //yukarı swipe da bisey yap
        if (laneNumVert == 0)
        {

            vertVel = 10;
            StartCoroutine(stopSlideVert());
            laneNumVert = 1;


        }

    }

    void OnSwipeDown()
    {
        //asagı swipe da bisey yap
        if (laneNumVert == 1)
        {

            vertVel = -10;
            StartCoroutine(stopSlideVert());
            laneNumVert = 0;


        }

    }

    void OnSwipeLeft()
    {
        //sag swipe da bisey yap

        if ((laneNum > 1) && (controlLocked == "n")) {
            horizVel = -12;

            StartCoroutine(stopSlide());
            /*
            inputL = true;
            */
            laneNum -= 1;
            controlLocked = "y";
        }

    }

    void OnSwipeRight()
    {
        //sol swipe da bisey yap

        if ((laneNum < 3) && (controlLocked == "n"))
        {
            horizVel = 12;

            StartCoroutine(stopSlide());
            /*
            inputR = true;
            */
            laneNum += 1;
            controlLocked = "y";
        }
    }
}
