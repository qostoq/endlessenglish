﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoadControl : MonoBehaviour {

    public GameObject[] tilePrefabs;
    private Transform PlayerTransform;

    private float spawnPointZ = -10f;
    public float prefabLength = 50f;
    public float safezone = 80f;
    public int tileOnScreen = 3;

    private int lastPrefabIndex = 0;
    


    //Ekranda görünen prefabları bir obje dizisinde tutar
    private List<GameObject> activeTiles;
    
    void Start () {

        activeTiles = new List<GameObject>();
        //player taglı objenin yanioyuncunun koordinatından baslayaraktile olusturulur. en basta 2 tane
        PlayerTransform = GameObject.FindGameObjectWithTag("Player").transform;
        for (int i = 0; i<tileOnScreen; i++)
        {
            if(i < 2)
            {
                spawnTile(0);
            }
            else
            {
                spawnTile();
            }
            
            
        }
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        //playerın z koordinatı - güvenli ayrılmıs bölge uzunlugu > spawnpoint -  ekrandaki prefab sayısı * prefab uzunlugu
        if (PlayerTransform.position.z -safezone > (spawnPointZ - tileOnScreen*prefabLength))
        {
            spawnTile();
            PlayerInGame.speed += 0.8f;
            deleteTile();
        }

         
    }

    public void spawnTile(int prefabIndex = -1)
    {
        GameObject roadkontrol;

        if (prefabIndex == -1)
        {
            roadkontrol = Instantiate(tilePrefabs[randomPrefabIndex()]) as GameObject;
            
        }
        else
        {
            roadkontrol = Instantiate(tilePrefabs[prefabIndex]) as GameObject;
            
        }

        roadkontrol.transform.SetParent(transform);
        roadkontrol.transform.position = Vector3.forward * spawnPointZ;
        spawnPointZ += prefabLength;
        //yolu diziye at
        activeTiles.Add(roadkontrol);

    }

    public void deleteTile()
    {
        //listenin ilk elemanını yok et ve diziden sil
        Destroy(activeTiles[0]);
        activeTiles.RemoveAt(0);
    }

    private int randomPrefabIndex()
    {

        if (tilePrefabs.Length <= 1)
            return 0;

        int randomIndex = lastPrefabIndex;
        while (lastPrefabIndex == randomIndex)
        {
            randomIndex = UnityEngine.Random.Range(0,tilePrefabs.Length);
        }
        lastPrefabIndex = randomIndex;
        return randomIndex;
    }

}
