﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour {

    public float zamanM;
    public static bool isMagnet;
    public AudioClip coinsound;
    public AudioSource soundsource;

    // Use this for initialization
    void Start () {
        soundsource.clip = coinsound;

    }
	
	// Update is called once per frame
	void FixedUpdate () {

        if (isMagnet == true && zamanM >= 0)
        {
            zamanM -= Time.deltaTime;
            
        }else
        {
            
            isMagnet = false;
            zamanM = 5;

        }

    }

    public void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Player")
        {
            soundsource.Play();
            isMagnet = true;
            Destroy(gameObject);
        }
    }
}
