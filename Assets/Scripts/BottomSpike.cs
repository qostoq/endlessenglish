﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BottomSpike : MonoBehaviour
{


    public Vector3 velocity;
    public bool triggerControl = false;
    public AudioClip coinsound;
    public AudioSource soundsource;

    // Use this for initialization
    void Start()
    {
        soundsource.clip = coinsound;
    }

    void OnTriggerEnter(Collider other)
    {

        triggerControl = true;
        soundsource.Play();

    }

    // Update is called once per frame
    void FixedUpdate()
    {



        if (transform.position.y <= 0 && triggerControl == true)
        {
            transform.Translate(velocity * Time.deltaTime);

        }
        
    }
}
