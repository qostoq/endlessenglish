﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Quiz : MonoBehaviour
{
    
    public GameObject ImageSoru,ImageA,ImageB,ImageC,ImageD,sonucImg,endgamepanel;
    public GameObject TimerText;
    
    public Image resim;
    

    public bool imageBool = false;
    public Text Soruİsmi, cevapa, cevapb, cevapc, cevapd, sorusayiyazi, zamanyazi, dogruCvpS,yanlisCvpS,Csay,Sonuctxt;
    public Text endgameDsayisi, endgameYsayisi, endgamePuan, endgameilan;

    Questions sr;

    public List<bool> sorulanlar;

    public int cevap, sorusayi, dogrucvpsayisi=0,yanliscvpsayisi=0,coinsayisi=0, counter=0,randtaban,randtavan;
    public float zaman;
    

    IEnumerator Co;
    IEnumerator Snc;

    void Start()
    {

        sr = GetComponent<Questions>();
        
        Co = Timer();
        Snc = SonucSn();
        sonucImg.gameObject.SetActive(false);

        for (int i = 0; i < sr.Sorular.Count; i++)
        {
            sorulanlar.Add(false);

        }
        SoruEkle();
        showHideImages();

    }

    void FixedUpdate()
    {
        if (yanliscvpsayisi == 3)
        {
            Time.timeScale = 0f;
            endgame();
        }

        if (sorusayi>24)
        {
            Time.timeScale = 0f;
        }
    }
    IEnumerator SonucSn()
    {
        
        yield return new WaitForSeconds(2.0f);
        sonucImg.gameObject.SetActive(false);

    }
    IEnumerator Timer()
    {
        
        while (zaman >= 0)
        {
            zaman -= Time.unscaledDeltaTime;
            zamanyazi.text = zaman.ToString("00");
            yield return null;

        }
        
            Debug.Log("Sure Doldu");
            sonucImg.gameObject.SetActive(true);
            Sonuctxt.text = "Sure Doldu!!!";
            zaman=15;
            PlayerInGame.speed += 10;
            StartCoroutine(Snc);
            SoruEkle();
            imageBool = false;
            showHideImages();
        
    }

    public void showHideImages()
    {
        
        ImageSoru.gameObject.SetActive(imageBool);
        TimerText.gameObject.SetActive(imageBool);
        ImageA.gameObject.SetActive(imageBool);
        ImageB.gameObject.SetActive(imageBool);
        ImageC.gameObject.SetActive(imageBool);
        ImageD.gameObject.SetActive(imageBool);
        resim.gameObject.SetActive(imageBool);

        
        

        if (imageBool==true)

        {
            sorusayi++;
            sorusayiyazi.text = "Sorulan Sorular: " + sorusayi;
            dogruCvpS.text = "Doğru Cevap Sayısı: " + dogrucvpsayisi;
            yanlisCvpS.text = "Yanlış Cevap Sayısı: " + yanliscvpsayisi;

            
            
                StartCoroutine(Co);
                Time.timeScale = 0f;
            
            
            
        }
        else
        {
            
            Time.timeScale = 1f;
            StopCoroutine(Co);
        }

        

    }
    public void SoruEkle()
    {
        for (int i = 0; i < sorulanlar.Count; i++)
        {
            if(sorusayi > 24)
            {
                Debug.Log("bitti");
                endgame();
            }
            else
            {
                if (sorusayi > 14)
                {
                    randtaban = 29;
                    randtavan = sorulanlar.Count;
                }
                else {
                    if (sorusayi > 9)
                    {
                        randtaban = 19;
                        randtavan = 29;
                    }
                    else
                    {
                        if (sorusayi > 4)
                        {
                            randtaban = 9;
                            randtavan = 19;
                        }
                        else
                        {
                            randtaban = 0;
                            randtavan = 9;
                        }

                    }


                }

            }

            if (sorulanlar[i] == false)
            {
                
                int sorusayi = Random.Range(randtaban, randtavan );
                if (sorulanlar[sorusayi] == false)
                {
                    sorulanlar[sorusayi] = true;
                    zaman = 15;
                    Soruİsmi.text = sr.Sorular[sorusayi].Soruismi;
                    cevapa.text = sr.Sorular[sorusayi].cevapa;
                    cevapb.text = sr.Sorular[sorusayi].cevapb;
                    cevapc.text = sr.Sorular[sorusayi].cevapc;
                    cevapd.text = sr.Sorular[sorusayi].cevapd;
                    cevap = sr.Sorular[sorusayi].cevap;
                    resim.sprite = sr.Sorular[sorusayi].Img;
                    
                }
                else
                {
                    SoruEkle();
                }

                break;
            }
            if (i == sorulanlar.Count - 1)
            {
                Debug.Log("oyunu kazandınız");
            }

        }



    }
    public void CevapVer(int deger)
    {

        if (deger == cevap)
        {
            StopAllCoroutines();
            dogrucvpsayisi++;
            dogruCvpS.text = "Dogru Cevap Sayisi: " + dogrucvpsayisi;
            
            SoruEkle();
            imageBool = false;
            sonucImg.gameObject.SetActive(true);
            showHideImages();
            Sonuctxt.text = "Dogru Cevap!!!";
            Debug.Log("Dogru cevap");
            StartCoroutine(Snc);
            if (PlayerInGame.speed > 35)
            {
                PlayerInGame.speed -= 5;
            }

        }
        else
        {
            Magnet.isMagnet = false;
            StopAllCoroutines();
            PlayerInGame.speed += 5;
            yanliscvpsayisi++;
            yanlisCvpS.text = "Yanlis Cevap Sayisi: " + yanliscvpsayisi;
            SoruEkle();
            imageBool = false;
            sonucImg.gameObject.SetActive(true);
            showHideImages();
            Sonuctxt.text = "Yanlis Cevap!!!";
            Debug.Log("yanlis cevap");
            StartCoroutine(Snc);

        }
    }
    public void endgame()
    {
        

        endgamepanel.gameObject.SetActive(true);

        if (yanliscvpsayisi == 3)
        {
            endgameilan.text = "3 Yanlis Cevap Verdiniz. \nSonuclar:";
        }
        else
        {
            endgameilan.text = "Test Bitti. \nSonuclar:";
        }

        endgameDsayisi.text = "" + dogrucvpsayisi;
        endgameYsayisi.text = "" + yanliscvpsayisi;
        endgamePuan.text = "" + coinsayisi;

        ImageSoru.gameObject.SetActive(false);
        TimerText.gameObject.SetActive(false);
        ImageA.gameObject.SetActive(false);
        ImageB.gameObject.SetActive(false);
        ImageC.gameObject.SetActive(false);
        ImageD.gameObject.SetActive(false);
        resim.gameObject.SetActive(false);

        sonucImg.gameObject.SetActive(false);
        dogruCvpS.gameObject.SetActive(false);
        yanlisCvpS.gameObject.SetActive(false);
        sorusayiyazi.gameObject.SetActive(false);
        Csay.gameObject.SetActive(false);

        
    }
}
